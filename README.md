# IngoGIT

Informacion importante sobre comandos GIT


COMANDOS GIT

FUENTE: https://www.solucionex.com/blog/borrar-ultimo-commit-con-reset-y-revert-en-git


**EJEMPLO 1:**

Solución si NO hemos subido el commit a nuestro repositorio remoto (no hemos realizado push):

1ª opción:
**git reset --hard HEAD~1**

--head: Con esta opción estamos indicando que retrocedemos a el comit HEAD~1 y perdemos todas las confirmaciones posteriores. HEAD~1 es un atajo para apuntar al commit anterior al que nos encontramos. CUIDADO, con la opcion –head, ya que como he dicho se borran todos los commits posteriores al commit al que indicamos.


**EJEMPLO 2:**

**git reset --soft HEAD~1 **
--soft: con esta opción estamos indicando que retrocedemos a el commit HEAD~1 y no perdemos los cambios de los commits posteriores. Todos los cambios aparecerán como pendientes para realizar un commit.

Solución si hemos subido el commit a nuestro repositorio remoto (hemos realizado push):
En caso de que queramos borrar un commit que ya hemos subido al servidor remoto, la mejor opcion es realizar un nuevo commit que borre el commit que queremos eliminar utilizando el comando revert. De esta forma cualquier usuario que se tenga actualizado el contenido del repositorio remoto puede obtener el cambio simplemente haciendo pull. Por lo tanto para borrar el ultimo commit teniendo en cuanta que el commit esta subido en un repositoriio remoto debemos usar:


git revert HEAD
